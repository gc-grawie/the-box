widthBasement = 150;
lengthBasement = 90;
heigthBasement = 3;
gapBeforeSides=3.75;
$fn=100;

module screwPlace() {
        cylinder(h=heigthBasement,r=1.5);

        translate([0, 0, heigthBasement - 1])
            cylinder(h=1,r1=1.5, r2=2.5);
//            cylinder(h=1,r=2.5); // old way
}

module basement() {
    difference() {
        cube([widthBasement,lengthBasement, heigthBasement]);

        
    translate([widthBasement/2,lengthBasement-gapBeforeSides, 0])
        screwPlace();
        
    translate([gapBeforeSides,lengthBasement/2, 0])
        screwPlace();

    translate([widthBasement/2,lengthBasement/2, 0])
        screwPlace();
        
    // moved due to led screen
    translate([widthBasement-35,gapBeforeSides, 0])
        screwPlace();

    // 55 + 7.5 => 9V socket gap
    translate([widthBasement-gapBeforeSides,lengthBasement-(55+7.5), 0])
        screwPlace();

    // 9V socket place
    translate([widthBasement - 20, lengthBasement-35-15, 0])
        cube([20,35 , heigthBasement]);

    }
}

module logbookWall() {
    difference() {
        //wall itself
        cube([85, 5, 30]);
            
        // notch at the top of wall
        translate([0, 0, 25]) 
            cube([85,2,3]);
    }
}

module logbookHousing() {
    // wall 1
    translate([0, lengthBasement-20, 0]) 
        logbookWall();
    
    // wall 2
    translate([0, lengthBasement-(50+20), 0]) 
    mirror([0, 1, 0]) logbookWall();
    
    // wall at the right/end of housing
     translate([80, lengthBasement-(50+20), 0]) 
        cube([5, 50, 25]);
}

basement();
logbookHousing();
